"""add new column for update date and status v3 05-Jul-2019

Revision ID: 1a62d5f9a7d5
Revises: 4a94a6c4d4d3
Create Date: 2019-07-05 00:41:20.157130

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1a62d5f9a7d5'
down_revision = '4a94a6c4d4d3'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('comment', sa.Column('status', sa.Boolean(), nullable=True))
    op.add_column('publish', sa.Column('date_updated', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('publish', 'date_updated')
    op.drop_column('comment', 'status')
    # ### end Alembic commands ###
