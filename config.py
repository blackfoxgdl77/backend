import os
from os.path import join, dirname, realpath

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'will_never_use'
    DB_USER = 'contractorbids' #root
    DB_PASS = 'Alonso587' #alonso21
    DB_HOST = 'contractorsbids.cwt1o1yfsrfm.us-east-2.rds.amazonaws.com' #'localhost'
    DB_NAME = 'contractorsbids'

    # CONNECTIONS
    SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{}:{}@{}/{}".format(DB_USER, DB_PASS, DB_HOST, DB_NAME)
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # JWT
    JWT_SECRET_KEY = 'secretStringForTest'
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

    # Security Token
    SECURITY_TOKEN_CB = "secretStringForTest"
    LOGIN_TOKEN_CB = "secretStringForTest"

    # single paths
    ADMIN_FILES   = 'static/img_admin/admin/'
    ADMIN_FILES_RESIZE = 'static/img_admin/admin_resize/'
    BANNERS_FILES = 'static/img_admin/banners/'
    BANNERS_FILES_RESIZE = 'static/img_admin/banners_resize/'
    BANNERS_SLIDERS = 'static/img_admin/banners_sliders/'
    BANNERS_SLIDERS_RESIZE = 'static/img_admin/banners_sliders_resize/'
    POSTS_FILES   = 'static/img_posts/'
    POSTS_FILES_RESIZE = 'static/img_posts_resize/'
    FILES = 'static/files/'

    # UPLOAD FILES
    APP_ROOT               = os.path.dirname(os.path.abspath(__file__))
    UPLOAD_ADMIN           = os.path.join(APP_ROOT, 'app/static/img_admin/admin/')
    UPLOAD_ADMIN_REZ       = os.path.join(APP_ROOT, 'app/static/img_admin/admin_resize/')
    UPLOAD_BANNERS         = os.path.join(APP_ROOT, 'app/static/img_admin/banners/')
    UPLOAD_BANNERS_REZ     = os.path.join(APP_ROOT, 'app/static/img_admin/banners_resize/')
    UPLOAD_BANNERS_SLIDERS = os.path.join(APP_ROOT, 'app/static/img_admin/banners_sliders/')
    UPLOAD_BANNERS_SLI_REZ = os.path.join(APP_ROOT, 'app/static/img_admin/banners_sliders_resize/')
    UPLOAD_POSTS           = os.path.join(APP_ROOT, 'app/static/img_posts/')
    UPLOAD_POSTS_REZ       = os.path.join(APP_ROOT, 'app/static/img_posts_resize/')
    FILES                  = os.path.join(APP_ROOT, 'app/static/files/')
    ALLOWED_EXTENSIONS     = ['jpg', 'jpeg', 'png', 'gif']

    # MESSAGES
    EXCEPTION_MESSAGE_ERROR   = 'The action can\'t be executed. Please contact contractorsbids Support Team.'
    EXCEPTION_INVALID_TOKENS  = 'Information invalid. Please log out and try to login, after this execute the action you want to.'
    EXCEPTION_MESSAGE_IMAGES  = 'The images can\'t be saves. Please try again or contact contractorsbids Support Team.'

    # MAIL CONFIG
    MAIL_SERVER   = 'localhost'
    MAIL_PORT     = 25
    MAIL_USERNAME = ''
    MAIL_PASSWORD = ''
