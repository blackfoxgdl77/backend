from app.users import users
from app.libraries.jwtFunctions import JwtFunctions
from flask import request, jsonify, json, make_response, current_app as app
import requests
from app.models import User, UserInfo, UserResetPassword, LoginHistory, ErrorLog, SecurityToken
from app.libraries.encryptInformation import EncryptInformation
from app import db
from datetime import datetime
from flask_jwt_extended import jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt

# method to save new user
@users.route('/api/auth/create', methods=['POST'])
@jwt_required
def auth_create_user():
    try:
        new_account  = request.get_json()
        bbb_accredit = None
        wcb          = None

        exists_account = User.query.filter_by(email=new_account['email']).count()

        if exists_account > 0:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The email already exists!' })
        else:
            if new_account['type_user'] == 0:
                bbb_accredit = None if new_account['accredited'] is None else new_account['accredited']
                wcb = None if new_account['wcb'] is None else new_account['wcb']

            new = User(name=new_account['name'],
                       email=new_account['email'],
                       username="TBD",
                       password=User().generate_password(new_account['password']),
                       is_company=new_account['is_company'], #1-Company, 0-User
                       type_user=new_account['type_user'], #0-Contrator, 1-Home Owner
                       is_active=2, #0-Inactive, 1-Active, 2-Verify Account
                       is_admin=0, #1-Admin, 0-User
                       activation_token='123123123',
                       is_confirm_token=0,
                       email_base=new_account['email'])

            db.session.add(new)
            db.session.commit()
            db.session.flush()

            new2 = UserInfo(phone=new_account['phone'],
                            bbb_accredited=bbb_accredit,
                            raiting=wcb,
                            info=new)
            db.session.add(new2)
            db.session.commit()
            db.session.flush()

            new.username = User().generate_username_dynamic(new.id)
            db.session.commit()

            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'tokens' : JwtFunctions().generate_token(new_account['email']),
                                 'data'   : {
                                    'id_user'      : User().encrypt_user_number(new.id, datetime.utcnow),
                                    'id_user_info' : User().encrypt_user_number(new2.id, datetime.utcnow),
                                    'user'         : new_account['name'],
                                    'message'      : 'The account has been created. Please verify the account before login.'
                                }})

        return response
    except Exception as e:
        print(e)
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

# method for login the platform
@users.route('/api/auth/login', methods=['POST'])
@jwt_required
def auth_login():
    try:
        credentials = request.get_json()
        user        = User.query.filter_by(username=credentials['user']).first()

        if user is None or not user.check_password(credentials['pwd']):
            response = jsonify({ 'code'    : 405,
                                 'status'  : 'ERROR',
                                 'message' : 'Username or Password are incorrect. Please verify your credentials.' })
        else:
            sec_token = SecurityToken(login_sec_token=SecurityToken().get_security_token(user.email, user.id, user.name),
                                      login_token=SecurityToken().get_login_token(user.email, user.username, user.id),
                                      sec_token=user)
            db.session.add(sec_token)
            db.session.commit()
            db.session.flush()

            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'token'  : JwtFunctions().generate_token(user.email),
                                 'data'   : {
                                    'user'    : user.username,
                                    'admin'   : user.is_admin,
                                    'message' : 'You have logged in successfully!',
                                    's_token' : {
                                        't1' : sec_token.login_sec_token,
                                        't2' : sec_token.login_token
                                    }
                                 }})

        return response
    except Exception as e:
        print(e)
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : 'Can not login in contractorsbids.com. Please contact to ContractorsBids Team.' })

@users.route('/api/users/<int:id>/info', methods=['GET'])
@jwt_refresh_token_required
def index(id):
    try:
        user     = UserInfo.query.filter_by(user_id=id).first()
        payment  = '' if (user.payment_method is None) else user.payment_method
        response = jsonify({ 'code'   : 200,
                             'status' : 'OK',
                             'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                             'data' : {
                                    'id'          : user.info.id,
                                    'username'    : user.info.username,
                                    'name'        : user.info.name,
                                    'email'       : user.info.email,
                                    'phone'       : user.phone,
                                    'is_active'   : user.info.is_active,
                                    'payment'     : payments_method_string(payment),
                                    'bbb_account' : user.bbb_account,
                                    'accredited'  : user.bbb_accredited,
                                    'raiting'     : user.raiting,
                                    'wcb_number'  : user.wcb_number,
                                    'address'     : user.address,
                                }
                            })

        return response
    except Exception as e:
        response = jsonify({ 'code'    : 400,
                             'status'  : 'ERROR',
                             'message' : "The user information can't be recovery" })
        return response

@users.route('/api/users/info/<int:id>/update', methods=['PUT', 'POST'])
@jwt_refresh_token_required
def update(id):
    value = request.get_json()

    try:
        users = UserInfo.query.filter_by(user_id=id).first()
        users.info.name      = value['name']
        users.info.email     = value['email']
        users.phone          = value['phone']
        users.payment_method = None if (value['payments'] is None) else payments_method_index(value['payments'])
        users.bbb_account    = value['bbb_account']
        users.bbb_accredited = value['accredited']
        users.raiting        = value['raiting']
        users.wcb_number     = value['wcb_number']
        users.address        = value['address']

        db.session.commit()

        return jsonify({ 'code'   : 200,
                         'status' : 'OK',
                         'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                         'data'   : {
                            'id'      : id,
                            'message' : 'Personal User Information has been updated successfully.'
                         }})
    except Exception as e:
        response = jsonify({ 'code'    : 400,
                             'status'  : 'ERROR',
                             'message' : 'The user information was not updated' })
        return response

@users.route('/api/users/<int:id>/delete', methods=['GET', 'DELETE'])
@jwt_refresh_token_required
def delete(id):
    try:
        users = User.query.filter_by(id=id).first()

        if users is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Wrong user. Please verify the user you want to delete.' })
        else:
            users.is_active = 2
            db.session.commit()

            response = jsonify({ 'code'    : 2,
                                 'status'  : 'OK',
                                 'message' : 'The user has been deleted successfully!',
                                 'header'  : 'User Deleted!',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'id'    : id
                                 }})

        return response
    except Exception as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

# method for reset password and sending email
@users.route('/api/users/reset/pwd', methods=['POST'])
def reset_password():
    emails = request.get_json()

    try:
        user = User.query.filter_by(email_base=emails['email']).first()

        if user is None:
            return jsonify({ 'code'    : '400',
                             'status'  : 'ERROR',
                             'message' : 'Email does not exists. Please verify the E-Mail you used for registration.' })

        # insert for reset pwd
        reset_pwd = UserResetPassword(token_reset=UserResetPassword().set_password_reset(emails['email'], datetime.utcnow),
                                      is_active=True,
                                      user_id=user.id,
                                      user_email=emails['email'])
        db.session.add(reset_pwd)
        db.session.commit()
        db.session.flush()

        return jsonify({ 'code'   : 200,
                         'status' : 'OK',
                         'data'   : {
                            'message' : 'The password has been reset. Please verify your email and follow the instructions.',
                            'user'    : user.id,
                            'email'   : user.email_base
                         }})

    except Exception as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : 'Can not reset the password. Please contact to ContractorsBids Team.' })

#history
@users.route('/api/users/login/<int:id>/history', methods=['POST'])
def save_login_history(id):
    try:
        history = request.get_json()

        if 'userN' in history:
            token = SecurityToken.query.filter_by(login_sec_token=history['userS'], login_token=history['userL'], token_user=history['userN'], status=1).first()
        else:
            token = SecurityToken.query.filter_by(login_sec_token=history['userS'], login_token=history['userL'], status=1).first()


        if token is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'There are some issues with your account. Please logout and try login again.' })
        elif id == 1:
            login_history = LoginHistory(token=history['token'],
                                         token_sec=token.login_sec_token,
                                         token_login=token.login_token,
                                         user_id=token.user_id)
            db.session.add(login_history)
            db.session.commit()
            db.session.flush()

            response = jsonify({ 'code'     : 200,
                                 'status'   : 'OK',
                                 'history'  : login_history.id,
                                 'message'  : 'Information has been stored successfully.' })
        else:
            hist = LoginHistory.query.filter_by(token_login=history['userL'], token_sec=history['userS'], status=1).first()
            login_update = LoginHistory.query.filter(LoginHistory.user_id==token.user_id).update(dict(status=0))
            db.session.commit()

            response = jsonify({ 'code'     : 200,
                                 'status'   : 'OK',
                                 'history'  : hist.id,
                                 'message'  : 'Information has been updated successfully.' })

        return response
    except Exception as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

# Method for updating data from sec
@users.route('/api/users/update/information', methods=['POST'])
def update_users_information():
    try:
        tokens  = request.get_json()
        history = LoginHistory.query.filter_by(id=tokens['history']).first()

        if history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'There are some issues with the login. Please logout and try to login again. If that does not works, please contact contractorsbids Support Team.' })
        else:
            sec = SecurityToken.query.filter_by(login_sec_token=tokens['userS'], login_token=tokens['userL'], status=1, user_id=history.user_id).first()

            sec.token_user = EncryptInformation().encrypt_user_id(history.user_id, history.date_login)
            db.session.commit()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The user has been logged in successfully.',
                                 'number'  : sec.token_user })

        return response
    except Exception as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : 'Can\'t login. Please contact contractorsbids Support Team.' })

# Method for get all the users
@users.route('/api/users/all', methods=['GET'])
@jwt_refresh_token_required
def get_all_users():
    try:
        headers = request.headers

        # get tokens from header
        _tokens = EncryptInformation().get_tokens_from_request(headers)

        security_token = SecurityToken.query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                       login_token=_tokens['_token_log'],
                                                       token_user=_tokens['_token_user'],
                                                       status=1).first()

        login_history  = LoginHistory.query.filter_by(id=_tokens['_token_adm'],
                                                      user_id=security_token.user_id,
                                                      status=1).first()
        users          = User.query.all()

        if security_token is None and login_history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        else:
            resp_users = [{ 'id'         : user.id,
                          'name'         : user.name,
                          'email'        : user.email,
                          'username'     : user.username,
                          'is_company'   : user.is_company,
                          'type_user'    : user.type_user,
                          'is_active'    : user.is_active,
                          'is_admin'     : user.is_admin,
                          'date_created' : user.date_created,
                          'date_updated' : user.date_updated,
                          'email_base'   : user.email_base } for user in users]

            response  = jsonify({ 'data'   : resp_users,
                                 'code'    : 200,
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'status'  : 'OK' })

        resp         = make_response(response)
        resp.headers = EncryptInformation().create_response_header(_tokens['_token_sec'], _tokens['_token_log'], _tokens['_token_user'], _tokens['_token_adm'])

        return resp
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : 'The users can\'t be recovery.' })
        return make_response(error)

# updates status records
@users.route('/api/users/users/update', methods=['POST'])
@jwt_refresh_token_required
def update_user_status():
    data = request.get_json()

    try:
        user = User.query.filter_by(id=data.user_id).first()
        user.is_active = data.user_status
        db.session.commit()
        action_done = "User inactive successfully." if data.user_status == 0 else "User active successfully."

        return jsonify({ 'code'   : 200,
                         'status' : 'OK',
                         'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                         'data'   : {
                            'header'  : action_done,
                            'message' : 'The record has been updated.',
                            'user_id' : user.id
                         }})
    except Exception as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : 'The data can\'t be updated.' })

# activate account
@users.route('/api/users/activate/<string:token_confirm>/account')
def activate_account_users(token_confirm):
    try:
        users = User.query.filter_by(activation_token=token_confirm, is_active=2).first()

        if users is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Token Invalid. Please verify the account is not active.',
                                })
        else:
            users.is_active = 1
            db.session.commit()

            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'data'   : {
                                    'user_id'  : users.id,
                                    'username' : users.username,
                                    'active'   : users.is_active,
                                    'message'  : 'The user has been activated successfully.'
                                 }})

        return response
    except Exception as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : 'Error in activate account action' })

# change password
@users.route('/api/user/update/password', methods=['POST', 'PUT'])
def change_passsword_users():
    changes = request.get_json()

    try:
        check_reset = UserResetPassword.query.filter_by(token_reset=changes['reset_token'], is_active=1, id=changes['reset_id']).first()

        if check_reset is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Token or user invalid. Please verify the email again or contact contractorsbids.com Support Team!' })
        else:
            check_reset.is_active = False

            users = User.query.filter_by(id=check_reset.user_id).first()
            users.password = User().generate_password(changes['password'])

            db.session.commit()
            db.session.flush()

            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'data'   : {
                                    'user_id'  : users.id,
                                    'username' : users.username,
                                    'message'  : 'The password has been changed successfully. Now you can login and starting to user contractorsbids.com'
                                 }})


        return response
    except Exception as e:
        print(e)
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : 'The password can\'t be updated. Please contact support team for resolve the issue.' })

# check valid token
@users.route('/api/users/valid/<int:id>/change', methods=['GET'])
def check_access_tokens(id):
    try:
        totals = UserResetPassword.query.filter_by(id=id, is_active=1).count()

        if totals > 0:
            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'data'   : {
                                    'valid'   : totals,
                                    'message' : 'Token Valid.'
                                 }})
        else:
            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'data'    : {
                                    'valid'   : totals,
                                    'message' : 'The token for change the password has expired or is invalid.' }})

        return response
    except Exception as e:
        print(e)
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : 'Error - Can\'t open the page, please contact contractorsbids.com Suppor Team.' })

# update users
@users.route('/api/availability/<int:id>/<int:status>/users', methods=['GET'])
def users_availability(id, status):
    try:
        user = User.query.filter_by(id=id).first()

        if user is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The user doesn\'t exists in the platform. Please contractorsbids Support Team.' })
        else:
            user.is_active = status
            db.session.commit()
            message = "The user {} has been {} successfully.".format(user.username, is_active_inactive(user.is_active))
            header  = "Account {}".format(is_active_inactive(user.is_active))

            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'data'   : {
                                    'message'  : message,
                                    'status'   : user.is_active,
                                    'username' : user.username,
                                    'header'   : header
                                 }})

        return response
    except Exceptions as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : 'The action in the status of the users can\'t be completed.' })

# logout
@users.route('/api/users/logout', methods=['POST'])
@jwt_required
def logout():
    try:
        data = request.get_json()

        # Add backlist tokens
        jti  = get_raw_jwt()['jti']
        blacklist.add(jti)

        security   = SecurityToken.query.filter_by(login_sec_token=data['userS'], login_token=data['userL'], token_user=data['userN'], status=1).first()
        update_sec = SecurityToken.query.filter(SecurityToken.user_id == security.user_id).update(dict(status = 0))
        db.session.commit()

        return jsonify({ 'code'    : 200,
                         'status'  : 'OK',
                         'message' : 'You have logged in successfully.' })
    except Exception as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

"""@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return jti in blacklist"""

def payments_method_string(value):
    options = {
        ''            : 0,
        'Cash'        : 1,
        'Credit Card' : 2,
        'Debit'       : 3,
        'E transfer'  : 4
    }

    return options.get(value)

def payments_method_index(value):
    options = {
        0 : '',
        1 : 'Cash',
        2 : 'Credit Card',
        3 : 'Debit',
        4 : 'E transfer'
    }

    return options.get(value)

def is_active_inactive(value):
    final_value = "Active" if value == 1 else "Inactive"

    return final_value
