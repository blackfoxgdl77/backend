from flask import request, jsonify, json, make_response, current_app as app
from app.libraries.jwtFunctions import JwtFunctions
from app.comments import comments
from app import db
from app.models import Comment, Publish, Post, SecurityToken, LoginHistory, CommentWinner, PublishWinner
import requests, os
from datetime import datetime
from flask_jwt_extended import jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt
from app.libraries.encryptInformation import EncryptInformation

# make a publish
@comments.route('/api/publish/comment', methods=['POST'])
@jwt_refresh_token_required
def make_a_publish():
    try:
        comments = request.get_json()
        header   = request.headers
        _tokens  = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()


        if _history is None or _security is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif comments is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The comment can\'t be posted. Please verify the comment was sent or contact contractorsbids Support Team.' })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            comment = Publish(user_publish_id=_security.user_id,
                              post_id=comments['post'],
                              title=comments['title'],
                              comment=comments['comment'])
            db.session.add(comment)
            db.session.commit();
            db.session.flush()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The comment has been published successfully',
                                 'header'  : 'Comment Published',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'id'        : comment.id,
                                    'title'     : comment.title,
                                    'comment'   : comment.comment,
                                    'post'      : comment.post_id,
                                    'name'      : comment.post_publish.post_name,
                                    'owner'     : comment.post_publish.user_post.username,
                                    'posted_by' : comment.user_publish.username,
                                    'date'      : comment.date_created
                                 }})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)
        return response

# make a winner commenr
@comments.route('/api/winner/publish/comment', methods=['POST'])
@jwt_refresh_token_required
def make_winner_comment():
    try:
        comment  = request.get_json()
        header   = request.headers
        _tokens  = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()

        if _history is None or _security is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif comments is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The comment can\'t be posted. Please verify the comment was sent or contact contractorsbids Support Team.' })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            winner = PublishWinner(title=comment['title'],
                                   comment=comment['comment'],
                                   project_id=comment['postId'],
                                   project_winner=_security.user_id)
            db.session.add(winner)
            db.session.commit();
            db.session.flush()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The comment has been published successfully',
                                 'header'  : 'Comment Published',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'id'        : winner.id,
                                    'title'     : winner.title,
                                    'comment'   : winner.comment,
                                    'post'      : winner.project_id,
                                    'name'      : winner.post_winner_publish.post_name,
                                    'owner'     : winner.post_winner_publish.user_post.username,
                                    'posted_by' : winner.user_winner_pub.username,
                                    'date'      : winner.date_created
                                 }})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)
        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)
        return response

# make a comment inside publish
@comments.route('/api/make/comment', methods=['POST'])
@jwt_refresh_token_required
def make_a_comment():
    try:
        body    = request.get_json()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        publishes = Publish.query.filter_by(id=body['publish']).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif publishes is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The comment does not exists. Please verify it or contact contractorsbids Support Team.' })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            comment = Comment(user_id=_security.user_id,
                              post_id=body['post'],
                              comments=body['comments'],
                              publish_id=body['publish'])

            db.session.add(comment)
            db.session.commit()
            db.session.flush()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The comment has been published successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'header'  : 'Comment Published.',
                                 'data'    : {
                                    'id'      : comment.id,
                                    'post'    : comment.post_id,
                                    'publish' : comment.publish_id,
                                    'comment' : comment.comments
                                 }})


        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)
        return response

# make a comment inside winner publish
@comments.route('/api/make/winner/comment', methods=['POST'])
@jwt_refresh_token_required
def make_a_winner_comment():
    try:
        body    = request.get_json()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        publishes = PublishWinner.query.filter_by(id=body['publish']).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif publishes is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The comment does not exists. Please verify it or contact contractorsbids Support Team.' })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            comment = CommentWinner(user_id=_security.user_id,
                                    post_id=body['post'],
                                    comments=body['comments'],
                                    publish_id=body['publish'])

            db.session.add(comment)
            db.session.commit()
            db.session.flush()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The comment has been published successfully.',
                                 'header'  : 'Comment Published.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'id'      : comment.id,
                                    'post'    : comment.post_id,
                                    'publish' : comment.publish_id,
                                    'comment' : comment.comments
                                 }})


        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)

        return final_response
    except Exception as e:
        print(e)
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })

        response = make_response(error)
        return response

# get publishes
@comments.route('/api/publish/<int:id>/comments/get', methods=['GET'])
@jwt_refresh_token_required
def get_all_comments(id):
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        publishes = Publish.query.filter_by(post_id=id, status=1).order_by(Publish.date_created.desc())

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        else:
            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'mesage' : 'Records has been recovery successfully',
                                 'header' : 'Records recovery successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'   : [
                                    {
                                        'id'      : publish.id,
                                        'user'    : publish.user_publish.username,
                                        'comment' : publish.comment,
                                        'title'   : publish.title,
                                        'created' : publish.date_created,
                                        'inner'   : Publish().get_comment_available(publish.id, 1)
                                    } for publish in publishes
                                 ]})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)

        return response

# get winner comment
@comments.route('/api/winner/publish/<int:id>/comments/get', methods=['GET'])
@jwt_refresh_token_required
def get_all_winner_comments(id):
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        publishes = PublishWinner.query.filter_by(project_id=id, status=1).order_by(PublishWinner.date_created.desc())

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif publishes is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'Data not found. Please check the post or if the issue persist, contat contractorsbids Suppor Team.' })
        else:
            response = jsonify({ 'code'   : 200,
                                 'status' : 'OK',
                                 'mesage' : 'Records has been recovery successfully',
                                 'header' : 'Records recovery successfully.',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'   : [
                                    {
                                        'id'      : publish.id,
                                        'user'    : publish.user_winner_pub.username,
                                        'comment' : publish.comment,
                                        'title'   : publish.title,
                                        'created' : publish.date_created,
                                        'inner'   : PublishWinner().get_winner_comment_available(publish.id, 1)
                                    } for publish in publishes
                                 ]})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        print(e)
        error    = jsonify({ 'code'    : 400,
                             'status'  : 'ERROR',
                             'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)
        return response

# report comments as fake
@comments.route('/api/report/<int:id>/fake/publish', methods=['POST', 'PUT'])
@jwt_refresh_token_required
def fake_publish(id):
    try:
        body    = request.get_json()
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        publishes = Publish.query.filter_by(id=id, post_id=body['post']).order_by(Publish.date_created.desc()).first()

        if _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        elif publishes is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The Comment does not exists or is wrong. Please contact contractorsbids Support Team.' })
        else:
            _token_sec = EncryptInformation().generate_token_sec(_security.sec_token.email, _security.user_id, _security.sec_token.name)
            _token_log = EncryptInformation().generate_token_login(_security.sec_token.email, _security.sec_token.username, _security.user_id)
            _token_num = EncryptInformation().encrypt_user_id(_security.user_id, _history.date_login)

            #update sec token table
            _security.login_sec_token = _token_sec
            _security.login_token     = _token_log
            _security.token_user      = _token_num
            db.session.commit()

            #update login history table
            _history.token_login = _token_log
            _history.token_sec   = _token_sec
            db.session.commit()

            publishes.status = False
            publishes.date_updated = datetime.utcnow()
            db.session.commit()

            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'Record has been reported successfully.',
                                 'header'  : 'Record Reported',
                                 'token'   : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                    'id'      : id,
                                    'post'    : publishes.post_id,
                                    'status'  : publishes.status,
                                    'updated' : publishes.date_updated
                                 }})

        final_response         = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_token_sec,
                                                                             _token_log,
                                                                             _token_num,
                                                                             _history.id)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        response = make_response(error)

        return response
