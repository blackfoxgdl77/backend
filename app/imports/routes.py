from flask import current_app as app
from app.imports import imports
from app.models import City, Country
import pandas as pd
from app import db

@imports.route('/fill/country/<int:id>/<string:country>', methods=['GET'])
def fill_country(id, country):
    name_file = "{}{}".format(app.config['FILES'], "worldcities.xlsx")
    data = pd.read_excel(name_file, sheet_name="Sheet1")
    df = pd.DataFrame(data)
    values = df.loc[df['iso3'] == country]
    country_id = Country.query.filter_by(id=id).first()

    for index,row in values.iterrows():
        countries = City(capital=row['admin_name'],
                            city_name=row['city'],
                            code=row['iso3'],
                            country=row['country'],
                            unique_id=row['id'],
                            latitude=row['lat'],
                            longitude=row['lng'],
                            country_id=country_id.id)
        db.session.add(countries)

    db.session.commit()

    return 'Cities inserted successfully!'
