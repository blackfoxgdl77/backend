from app.websites import websites
from flask import request, jsonify, json, make_response, current_app as app
import requests, os
from app.models import DynamicData, Post, SecurityToken, LoginHistory
from app import db
from app.libraries.encryptInformation import EncryptInformation

@websites.route('/api/test')
def test():
    return "olas"

# banners
@websites.route('/api/website/banners', methods=['GET'])
def banners():
    try:
        data = {}

        data = DynamicData.query.filter(DynamicData.section == 1, DynamicData.status == 1)
        response = jsonify({ 'code'   : 200,
                             'status' : 'OK',
                             'data'   : [
                                {
                                    'id'             : banners.id,
                                    'section'        : banners.section,
                                    'file'           : banners.file,
                                    'file_size'      : banners.file_size,
                                    'file_sliders'   : banners.file_slider,
                                    'original_name'  : banners.original_name,
                                    'url'            : banners.url,
                                    'file_extension' : banners.file_extension,
                                    'description'    : banners.description,
                                    'date_created'   : banners.date_created,
                                    'status'         : banners.status,
                                    'type_file'      : banners.type_file
                                } for banners in data]})

        final_response = make_response(response)
        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

# terms and conditions
@websites.route('/api/website/termsConditions', methods=['GET'])
def conditions():
    try:
        data = {}

        data = DynamicData.query.filter(DynamicData.section == 4, DynamicData.status == 1)
        response = jsonify({ 'code'   : 200,
                             'status' : 'OK',
                             'data'   : [
                                {
                                    'id'          : conditions.id,
                                    'section'     : conditions.section,
                                    'description' : conditions.description,
                                    'date_created': conditions.date_created,
                                    'status'      : conditions.status,
                                    'type_file'   : conditions.type_file
                                } for conditions in data]})

        final_response = make_response(response)

        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

# about us
@websites.route('/api/website/aboutUs', methods=['GET'])
def about():
    try:
        data = {}

        data = DynamicData.query.filter(DynamicData.section == 3, DynamicData.status == 1)
        response = jsonify({ 'code'   : 200,
                             'status' : 'OK',
                             'data'   : [
                                {
                                    'id'          : section.id,
                                    'section'     : section.section,
                                    'description' : section.description,
                                    'date_created': section.date_created,
                                    'status'      : section.status,
                                    'type_file'   : section.type_file,
                                    'file'        : section.file,
                                    'file_resize' : section.file_size,
                                    'url'         : section.url
                                } for section in data]})

        final_response = make_response(response)
        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

# how it works
@websites.route('/api/website/howItWorks', methods=['GET'])
def works():
    try:
        data = {}

        data = DynamicData.query.filter(DynamicData.section == 2, DynamicData.status == 1)
        response = jsonify({ 'code'   : 200,
                             'status' : 'OK',
                             'data'   : [
                                {
                                    'id'          : section.id,
                                    'section'     : section.section,
                                    'description' : section.description,
                                    'date_created': section.date_created,
                                    'status'      : section.status,
                                    'type_file'   : section.type_file,
                                    'file'        : section.file,
                                    'file_resize' : section.file_size,
                                    'url'         : section.url
                                } for works in data]})

        final_response = make_response(response)
        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response

# contact us
@websites.route('/api/website/contactUs', methods=['POST'])
def contact():
    return ''

# recent projects
@websites.route('/api/website/recent/projects', methods=['GET'])
def recent_projects():
    try:
        posts = Post.query.filter_by(post_status=1).order_by(Post.id.desc()).limit(6).all()

        response = jsonify({ 'code'    : 200,
                             'status'  : 'OK',
                             'message' : 'The most recent posts has been recovery.',
                             'data'    : [
                                {
                                    'id'          : post.id,
                                    'name'        : post.post_name,
                                    'description' : post.description,
                                    # TODO: Change the img by a dynamic image
                                    'image'       : 'http://localhost:5000/static/img_admin/banners_sliders/e49fda4b3369f071d50fb92e560ccda9.png',
                                    'category'    : post.post_category.name,
                                    'city'        : post.post_cities.city_name,
                                    'country'     : post.post_cities.country_city.name_country
                                } for post in posts]})

        final_response = make_response(response)
        return final_response
    except Exception as e:
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response
