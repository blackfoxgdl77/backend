from flask import Blueprint

websites = Blueprint('websites', __name__)

from app.websites import routes
