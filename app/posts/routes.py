from app.posts import posts
from app.libraries.jwtFunctions import JwtFunctions
from flask import jsonify, json, make_response, request, current_app as app
import requests
from app.models import Post, SecurityToken, LoginHistory
from app import db
from datetime import datetime
from flask_jwt_extended import jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt
from app.libraries.encryptInformation import EncryptInformation

@posts.route('/api/posts/search', methods=['GET'])
@jwt_refresh_token_required
def get_posts():
    data = {}

    try:
        data = Post.query.all()
        resp = [
                { 'id'             : record.id,
                  'post_name'      : record.post_name,
                  'description'    : record.description,
                  'post_payment'   : record.post_payment,
                  'category'       : record.category,
                  'city'           : record.city,
                  'status'         : record.post_status,
                  'type_file'      : record.type_file,
                  'url'            : record.url,
                  'file'           : record.file,
                  'file_size'      : record.file_size,
                  'file_extension' : record.file_extension,
                  'original_name'  : record.original_name } for record in data]

        return jsonify({ 'data'   : resp,
                         'token'  : JwtFunctions().refresh_token(get_jwt_identity()),
                         'code'   : 200,
                         'status' : 'OK' })
    except Exception as e:
        print(e)
        return jsonify({ 'code'   : 400,
                         'status' : 'ERROR',
                         'message': "Can't get data from the system." })

@posts.route('/api/posts/search/user/<int:id>', methods=['GET'])
@jwt_refresh_token_required
def get_posts_by_user(id):
    data = {}

    try:
        data = Post.query.filter_by( user = id )
        resp = [{'id': record.id,
                 'post_name': record.post_name,
                 'description': record.description,
                 'post_payment': record.post_payment,
                 'category': record.category,
                 'city': record.city,
                 'status': record.post_status,
                 'type_file': record.type_file,
                 'url': record.url,
                 'file': record.file,
                 'file_size': record.file_size,
                 'file_extension': record.file_extension,
                 'original_name': record.original_name} for record in data]

        return jsonify({'data': resp,
                        'token' : JwtFunctions().refresh_token(get_jwt_identity()),
                        'code': 200,
                        'status': 'OK'})
    except Exception as e:
        return jsonify({'code': 400,
                        'status': 'ERROR',
                        'message': "Can't get data from the system."})

@posts.route('/api/posts/search/category/<int:id>', methods=['GET'])
@jwt_refresh_token_required
def get_posts_by_category(id):
    data = {}

    try:
        data = Post.query.filter_by( category = id )
        resp = [{'id': record.id,
                 'post_name': record.post_name,
                 'description': record.description,
                 'post_payment': record.post_payment,
                 'category': record.category,
                 'city': record.city,
                 'status': record.post_status,
                 'type_file': record.type_file,
                 'url': record.url,
                 'file': record.file,
                 'file_size': record.file_size,
                 'file_extension': record.file_extension,
                 'original_name': record.original_name} for record in data]

        return jsonify({'data': resp,
                        'token' : JwtFunctions().refresh_token(get_jwt_identity()),
                        'code': 200,
                        'status': 'OK'})
    except Exception as e:
        return jsonify({'code': 400,
                        'status': 'ERROR',
                        'message': "Can't get data from the system."})

@posts.route('/api/posts/create', methods=['POST'])
@jwt_refresh_token_required
def create_post():
    print("holas")
    record = request.get_json()
    print(record)
    try:
        post = Post(post_name=record['postname'],
                    description=record['description'],
                    post_payment=float(record['payment']),
                    city_id=record['city'],
                    category_id=record['category'],
                    post_status=record['status'],
                    user_id=1)
                    #type_file=record['options'],
                    #url=record['urlVideo'])
        print(post)
        db.session.add(post)
        db.session.commit()
        db.session.flush()

        return jsonify({'code': 200,
                        'status': 'OK',
                        'token' : JwtFunctions().refresh_token(get_jwt_identity()),
                        'data': {
                            'id': post.id,
                            'message': 'The record has been saved successfully!'
                        }})

    except Exception as e:
        print(e)
        response = jsonify({'code': 400,
                            'status': 'ERROR',
                            'message': 'Error to add data'})
        return response

@posts.route('/api/posts/update/<int:id>', methods=['POST'])
@jwt_refresh_token_required
def update_posts(id):
    record = request.get_json()

    try:

        post = Post.query.filter_by( id=id ).first()

        post.post_name    = record['postname'],
        post.description  = record['description'],
        post.payment      = record['payment'],
        post.city         = record['city'],
        post.category     = record['category'],
        post.status       = record['status'],
        post.type_file    = record['options'],
        post.url          = record['urlVideo']
        post.type_file    = record['options']
        post.date_updated = datetime.utcnow()
        post.url          = record['urlVideo']
        db.session.commit()

        return jsonify({ 'code'   : 200,
                         'status' : 'OK',
                         'token' : JwtFunctions().refresh_token(get_jwt_identity()),
                         'data'   : {
                            'message' : 'Record updated successfully',
                            'id'      : id
                         }})

    except Exception as e:
        return jsonify({ 'code'    : 400,
                         'status'  : 'ERROR',
                         'message' : "The record can't be updated." })

# get single code
@posts.route('/api/posts/<int:id>/get', methods=['GET'])
@jwt_refresh_token_required
def get_post(id):
    try:
        header  = request.headers
        _tokens = EncryptInformation().get_tokens_from_request(header)

        _security = SecurityToken().query.filter_by(login_sec_token=_tokens['_token_sec'],
                                                    login_token=_tokens['_token_log'],
                                                    token_user=_tokens['_token_user'],
                                                    status=1).first()
        _history  = LoginHistory().query.filter_by(id=_tokens['_token_adm'],
                                                   user_id=_security.user_id,
                                                   status=1).first()
        record    = Post.query.filter_by(id=id).first()

        if record is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : 'The post does not exists in contractorsbids.' })
        elif _security is None or _history is None:
            response = jsonify({ 'code'    : 400,
                                 'status'  : 'ERROR',
                                 'message' : app.config['EXCEPTION_INVALID_TOKENS'] })
        else:
            response = jsonify({ 'code'    : 200,
                                 'status'  : 'OK',
                                 'message' : 'The post has been recovery successfully.',
                                 'token' : JwtFunctions().refresh_token(get_jwt_identity()),
                                 'data'    : {
                                        'id'           : record.id,
                                        'name'         : record.post_name,
                                        'description'  : record.description,
                                        'payment'      : record.post_payment,
                                        'category'     : record.post_category.name,
                                        'city'         : record.post_cities.city_name,
                                        'country'      : record.post_cities.country_city.name_country,
                                        'username'     : record.user_post.username,
                                        'owner'        : _security.sec_token.username,
                                        'status'       : record.post_status,
                                        'created_at'   : record.date_created
                                    }})

        final_response = make_response(response)
        final_response.headers = EncryptInformation().create_response_header(_security.login_sec_token,
                                                                             _security.login_token,
                                                                             _security.token_user,
                                                                             _history.id)

        return final_response
    except Exception as e:
        print(e)
        error = jsonify({ 'code'    : 400,
                          'status'  : 'ERROR',
                          'message' : app.config['EXCEPTION_MESSAGE_ERROR'] })
        final_response = make_response(error)
        return final_response
