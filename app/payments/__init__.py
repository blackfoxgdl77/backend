from flask import Blueprint

payments = Blueprint('payments', __name__)

from app.payments import routes
