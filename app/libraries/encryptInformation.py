""" Class will contain all the methods for using
    to encrypt information required by the user.
    Should be called and just passing parameters will create
    all the process
"""

from flask import current_app as app
from app import db
from app.models import SecurityToken, User

class EncryptInformation(object):

    def generate_token_sec(self, email, user, name):
        return SecurityToken().get_security_token(email, user, name)

    def generate_token_login(self, email, username, user):
        return SecurityToken().get_login_token(email, username, user)

    def encrypt_user_id(self, user, login_date):
        return User().encrypt_user_number(user, login_date)

    def verify_token_login(self, token, validation):
        if token == validation:
            return True

        return False

    def verify_token_sec(self, token, validation):
        if token == validation:
            return True

        return False

    def verify_user_number(self, usertoken, validation):
        if usertoken == validation:
            return True

        return False

    def create_response_header(self, user_sec, user_log, user_num, user_adm):
        tokens = { '_token_sec'   : user_sec,
                   '_token_log'   : user_log,
                   '_token_user'  : user_num,
                   '_token_adm'   : user_adm,
                   'Content-Type' : 'application/json' }

        return tokens

    def get_tokens_from_request(self, obj_token):
        _get_tokens = { '_token_sec'  : obj_token.get('_token_sec'),
                        '_token_log'  : obj_token.get('_token_log'),
                        '_token_user' : obj_token.get('_token_user'),
                        '_token_adm'  : obj_token.get('_token_adm') }

        return _get_tokens
