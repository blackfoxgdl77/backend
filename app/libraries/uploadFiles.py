""" Files will be used for all the functions
    needed at the moment to upload files
    or anything"""

from flask import current_app as app
from datetime import datetime
import hashlib, os

class UploadFiles(object):

    # Check extension
    def allowed_extensions(self, filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

    # get extension
    def get_extension_file(self, filename):
        return filename.rsplit('.', 1)[1].lower()

    # Get new filename of file
    def new_filename(self, id, finalname, section, is_resize):
        name  = '{}{}{}{}{}'.format(datetime.now(), app.config['SECRET_KEY'], finalname, id, section) if is_resize == 0 else '{}{}{}{}{}_resize'.format(datetime.now(), app.config['SECRET_KEY'], finalname, id, section)
        final = hashlib.md5(name.encode('utf-8'))

        return final.hexdigest()

    # Get path for resize images in admin section
    def path_to_save_db_resize(self, section):
        return app.config['BANNERS_FILES_RESIZE'] if section == 1 else app.config['ADMIN_FILES_RESIZE']

    # Get path for images in admin section
    def path_to_save_db(self, section):
        return app.config['BANNERS_FILES'] if section == 1 else app.config['ADMIN_FILES']

    # Get path for images in banners sliders
    def path_to_save_db_sliders(self):
        return app.config['BANNERS_SLIDERS']

    def remove_files_from_server(self, id): #, section, file1, file2):
        if id == 1:
            paths = UploadFiles().upload_variables(1)
            print(paths['upload_first'])
            #if os.path.exists(os.path.join(paths, file1)):
            #    print("holas")


    def upload_variables(self, section):
        if section == 1:
            apps = { 'upload_first'  : app.config['UPLOAD_BANNERS'],
                     'upload_second' : app.config['UPLOAD_BANNERS_REZ'],
                     'upload_three'  : app.config['UPLOAD_BANNERS_SLIDERS'] }

        return apps
