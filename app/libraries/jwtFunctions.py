""" File will contains functions to generate the
    tokens and refresh to return via API"""

from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required,
                                get_jwt_identity, get_raw_jwt)

class JwtFunctions(object):

    def generate_token(self, info_data):
        create_token  = create_access_token(identity=info_data)
        refresh_token = create_refresh_token(identity=info_data)

        return { 'create_token'  : create_token,
                 'refresh_token' : refresh_token }

    def refresh_token(self, info_data):
        create_new_token  = create_access_token(identity=info_data)
        refresh_new_token = create_refresh_token(identity=info_data)

        return { 'create_token'  : create_new_token,
                 'refresh_token' : refresh_new_token }
