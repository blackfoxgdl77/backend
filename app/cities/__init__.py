from flask import Blueprint

cities = Blueprint('cities', __name__)

from app.cities import routes
